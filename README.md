cyr2lat
=======


## About

This is a simple transliteration script, which converts letters from the Cyrillic script into the Latin script according to the simplest streets and road signs [romanization rule](Street and road signs).

The script uses either `gawk`, if it's available, or `sed`. Please, be aware, that `mawk`, which is the default AWK interpreter on some systems, [doesn't](https://github.com/ThomasDickey/original-mawk/issues/62) [support](https://github.com/ThomasDickey/original-mawk/issues/10) Unicode.
